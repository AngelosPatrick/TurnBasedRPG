![](https://img.shields.io/badge/Microverse-blueviolet)

# Spiders

Spiders is a turn-based, RPG where the goal of the game is to collect as many coins as you can on a farm full of spiders.
Each time you collect a coin a new spider spawns on the farm.
You can submit your name before the game begins and compete for a high score.
The UI on the game scene shows the stats of the player (Health, Attack, Defence).
Although the attack and defense are not changing the idea was to increase them every few coins the player collected.

Move with: WASD keys.

For more detailed info check the [game design](./docs/gameDesign.txt) fiel

![](./docs/spiders.png)

## [Live Link](https://quizzical-aryabhata-55d239.netlify.app/)

## Built With

- JavaScript
- Phaser

## Getting Started

To get a local copy running

- `git clone git@github.com:patrick-angelos/TurnBasedRPG.git`
- `cd TurnBasedRPG`
- `npm install`
- `npm run start`

To run the test

- `npm test`

## Author

👤 **Patrikis Angelos**

- GitHub: [@patrick-angelos](https://github.com/patrick-angelos)
- Twitter: [@AngelosPatrikis](https://twitter.com/AngelosPatrikis)
- LinkedIn: [Angelos Patrikis](https://www.linkedin.com/in/angelos-patrikis-a590a61b5/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- To [Henrique Lazarini (7Soul1)](https://www.deviantart.com/7soul1) for his great collection of pixel art .
- To Luis Zuno (@ansimuz) for his great map tiles.
- To Microverse for their `README` template.

## 📝 [License]

MIT License Copyright (c) 2021 Andres Ortegon Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE